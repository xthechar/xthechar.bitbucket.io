<link rel="stylesheet" href="https://unpkg.com/sakura.css/css/sakura.css" type="text/css">
<style>
  body {
    max-width: 64em;
  }
</style>

## How to use Mudfish to improve your connection on Slippi Netplay

First download Mudfish from <https://mudfish.net/download>. Pick the download for your relevant
browser under the "Cloud VPN" section. You probably want the Windows 64 bit download.

![Mudfish download page.](img/mudfish01.png)

Once you download Mudfish, it should be in your Downloads folder as
`mudfish-5.2.7-x86_64-win2k-setup.exe`. Right-click the exe and select "Run as admin". Mudfish
requires admin privileges because it changes your internet routing, but don't worry, it's totally
safe, it's a trustworthy and reputable program and many of use have used it before.

A browser window should open with the main Mudfish page. If it doesn't, go to your browser and type
`127.0.0.1:8282` into the address bar and hit "enter". It should take you to the main Mudfish page.
This is a program running on your computer that you simply view through the browser.

![Main Mudfish page in the browser.](img/mudfish02.png)

Log into Mudfish using the username and password provided through the Discord. Once you've logged
in, hit "Connect" to start Mudfish. Make sure you aren't streaming from ten billion Twitch pages at
once, as it will start to consume the credits once it connects.

![Mudfish started. Drop-down for "Status" menu...](img/mudfish03.png)

Open the "Status" menu by clicking it, and then click "Nodes". You should see a list of nodes with a
table showing something called "RTT", which is your ping to the nodes. It may take a while if it's
your first time opening Mudfish, but in a couple minutes max it should fill in the whole table.
Click the arrow on the "RTT Avg." column until only the up-arrow is showing, so it sorts by ping
from lowest to highest. Now you can see the nodes that you have the best ping to.

![Nodes sorted by "RTT Avg." (i.e. ping).](img/mudfish04.png)

After doing that, you can even filter by country (or by anything in the "Name" column). Notice how
the nodes for a given country use the "country code" in the name. So to search for nodes in Japan,
type "JP" into the search box at the top-right, as shown in the screenshot below. Or, type "KR" to
search for nodes in Korea, "HK" to serach for nodes in Hong Kong, "TW" for nodes in Taiwan, etc.

![Sorted nodes filtered by "JP".](img/mudfish05.png)

Pick the node you want to connect to. When determining which node to use to best minimize the ping
to your opponent, use either 1) a node in their country, 2) a node in a country with very good
connectivity that is somewhere in-between you and your opponent, like Hong Kong, Taiwan, Japan, or
Singapore, or maybe even a node in your country -- especially if you think the ping would be fine
but you're looping. Pick the lowest ping to try first, and try a few of the lowest pings even if the
first one doesn't work out. There are so many nodes, one of them is bound to work very well.

Click the Mudfish VPN text in the top left to go back to the main menu, and click "Disconnect" to
turn off the VPN at this point. When we re-enable it, we will be using the node we selected in the
"Full VPN" mode so that Slippi data will be routed through our new node.

![Drop-down for "Setup" menu...](img/mudfish06.png)

Once you decide on your node, open the "Setup" menu and click "Program". Then click on the "Full
VPN" tab.

!["Setup" menu on "Full VPN tab.](img/mudfish07.png)

You should see a drop-down menu under "Mudfish Nodes" -- this is where you pick the node you just
decided to use. So pick it and click "Save" to save that setting.

![Selecting "TW Asia (Taiwan - Google 6) node.](img/mudfish08.png)

Click back again on the Mudfish VPN text in the top left to go to the main menu, and click "Connect"
to re-enable the VPN with the new settings. Under the "Status" panel you should see "Full VPN Mode"
shown as enabled. Wait until the bar at the top shows "100% Completed" and you should now be using
the VPN node that was selected.

![Re-connecting in Full VPN mode with our selected node.](img/mudfish09.png)

As a final step, click the circular arrow icon next to "Current Public IP" to ensure that you are
connected with the new IP. You can double-check this with the node's IP address if you like. The
easiest way to ensure you're on the new vpn is to go to <https://myip.mudfish.net> and it'll tell
you if you're connected to the proper node.

![Refreshed "Current Public IP".](img/mudfish10.png)

And that's it! You don't even have to close Slippi, just start re-connecting in the "Direct" or
"Unranked" mode, and you will connect through the VPN and have the glorious connection to anywhere
in the region, that you've always dreamed of.
